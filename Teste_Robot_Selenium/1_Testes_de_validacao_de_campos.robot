***Settings***
Documentation       Cenários de testes do cadastro no site americanas.com
Library     SeleniumLibrary
Library     FakerLibrary
Library     BuiltIn
Library     ./CPF_GENERATOR.py
Resource    ./Resource_Keywords.robot

***Test Cases***
Teste de E-mail ja cadastrado
    Abrir Sessao    ${BROWSERS.CHROME}
    Fechar alerta de cookie
    Abrir cadastro de usuario
    Preencher cadastro com e-mail ja cadastrado
    Fechar sessao 
    
Teste de Senha Fraca
    Abrir Sessao    ${BROWSERS.CHROME}
    Fechar alerta de cookie
    Abrir cadastro de usuario
    Preencher cadastro com senha fraca
    Fechar sessao 

Teste de CPF invalido
    Abrir Sessao    ${BROWSERS.CHROME}
    Fechar alerta de cookie
    Abrir cadastro de usuario
    Preencher cadastro com CPF invalido 
    Fechar sessao 