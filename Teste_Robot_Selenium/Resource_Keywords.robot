***Settings***
Documentation       Diretorio de implementação de Keywords
Library     SeleniumLibrary
Library     FakerLibrary
Library     BuiltIn
Library     String
Library     ./CPF_GENERATOR.py


*** Keywords ***

Abrir Sessao
    [Arguments]         ${Browser}
    Open Browser        ${URLS.HOMEAMERICANAS}        ${Browser}        options=add_argument("--incognito")

Abrir cadastro de usuario
    Wait Until Page Contains Element       id:h_usr-link        5    
    Click Element                          id:h_usr-link
    sleep                                  2s
    Click Link                             ${URLS.CADASTROAMERICANAS}
Ir para HomePage
    Click Link                             id:brd-link 

Preencher cadastro com e-mail ja cadastrado
    Wait Until Page Contains Element       id:email-input        5 
    Input Text                             id:email-input       teste@hotmail.com
    Input Text                             id:password-input      ab
    sleep                                  2s
    Click Button                           Criar seu cadastro
    sleep                                  2s
    Element Text Should Be                 css:.inputGroup-error     E-mail já cadastrado

Preencher cadastro com senha fraca
    Wait Until Page Contains Element       id:email-input       5 
    ${emailFake}=                          FakerLibrary.Email
    Input Text                             id:email-input       ${emailFake}
    Input Text                             id:password-input    ab
    sleep                                  2s
    Element Text Should Be                 css:.passwordWrapper-msg    fraca

Preencher cadastro com CPF invalido
    Wait Until Page Contains Element       id:email-input        5 
  
    ${emailFake}=    FakerLibrary.Email
    ${nome}=     FakerLibrary.Name   
    ${sobreNome}=     FakerLibrary.Last Name               
    ${telefone}=     FakerLibrary.Phone Number   

    Input Text      id:email-input       ${emailFake}
    Input Text      id:password-input      1234567
    Input Text      id:cpf-input      12345678910
    Input Text      id:name-input       teste cpf invalido
    Input Text      id:birthday-input       01012000
    #Click Image     ${IMGPATH}/Radio.png
    Input Text      id:phone-input      48958745895
    sleep       2s
    Click Button        Criar seu cadastro
    sleep       2s
    Element Text Should Be       css:.inputGroup-error    CPF inválido.

Preencher cadastro
    Wait Until Page Contains Element       id:email-input        5 
  
    ${emailFake}=    FakerLibrary.Email
    ${cpf}=      CPF_GENERATOR.generate_cpf     0
    ${nome}=     FakerLibrary.Name   
    ${sobreNome}=     FakerLibrary.Last Name               
    ${telefone}=     FakerLibrary.Phone Number   

    Input Text      id:email-input       ${emailFake}
    Input Text      id:password-input      ab
    Input Text      id:cpf-input      ${cpf}
    Input Text      id:name-input       isaak iedo
    Input Text      id:birthday-input       01012000
    #Click Image     ${IMGPATH}/Radio.png
    Input Text      id:phone-input      48958745895
    sleep       2s
    Click Button        Criar seu cadastro



Selecionar primeiro produto
    Input Text           id:h_search-input       Moto G6
    Click Button         id:h_search-btn
    Sleep       5s                     
    Click Element        xpath:/html/body/div/div/div/div[3]/div[3]/div[1]/div/a/span[1] 
    Wait Until Page Contains Element        id:buy-button
    Click Element        id:buy-button
    Wait Until Page Contains Element        id:btn-continue
    Click Button         id:btn-continue
    
Selecionar segundo produto
    Input Text           id:h_search-input       Moto G6
    Click Button         id:h_search-btn
    Sleep       5s
    Click Element        xpath:/html/body/div/div/div/div[3]/div[3]/div[2]/div/a/span[1]
    Wait Until Page Contains Element        id:buy-button
    Click Element        id:buy-button
    Wait Until Page Contains Element        id:btn-continue
    Click Button         id:btn-continue


Validar valor total
    Wait Until Element Is Visible       xpath:/html/body/div[4]/section/section/div[2]/div/div[1]/span[2]       15      Valor total nao foi apresentado em 15 segundos
    log to console           validando dados de valor e parcela
    ${valorTotal}=      Get Text       xpath:/html/body/div[4]/section/section/div[2]/div/div[1]/span[2]
    log to console       string valor total eh: ${valorTotal}
    ${ValorSubstring}=     Get Substring       ${valorTotal}       3       11
    ${RemoveVirgula}=       Remove string       ${ValorSubstring}       ,
    ${RemovePonto}=         Remove string       ${RemoveVirgula}       .
    log to console      valor sem virgula eh: ${RemoveVirgula}
    log to console      valor sem ponto eh: ${RemovePonto}
    ${ValorConvertToNumber}=        Convert To Number	     ${RemoveVirgula}
    log to console      valor total depois da substring eh: ${ValorSubstring}
    log to console      valor total depois da conversao para number eh: ${ValorConvertToNumber}
    Run Keyword if     ${ValorConvertToNumber}>5.000       Valor maior que 5 mil
   
Validar parcelas   
    Element Should Contain      css:.summary-totalInstallments      12x sem juros

Valor maior que 5 mil
    Fail   Teste quebrou, valor total maior que 5 mil reais

Fechar alerta de cookie
    Wait Until Page Contains Element       id:lgpd-accept        5
    Click Button                           id:lgpd-accept

Fechar sessao 
    Close Browser  



*** Variables ***
&{URLS}                     HOMEAMERICANAS= https://www.americanas.com.br 
...                         CADASTROAMERICANAS=https://cliente.americanas.com.br/simple-login/cadastro/pf?next=https%3A%2F%2Fwww.americanas.com.br%2F

&{BROWSERS}                 CHROME= chrome
...                         FIREFOX= firefox
...                         IE= internetexplorer
...                         SAFARI= safari
...                         OPERA= opera

&{EMAILS}                  EXISTENTE = teste@hotmail.com
...                        FAKE= FakerLibrary.Email

#${IMGPATH}                  ./Imagens