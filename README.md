Teste_Robot_Selenium
===============


Introdução
------------

Esse é um projeto de automação WEB utilizando `Robot Framework <http://robotframework.org>` com `Selenium <https://github.com/robotframework/SeleniumLibrary/>`

Pré condição
------------

Ter a Versão 3.6 do `Python <https://www.python.org/>` instalada.
Ter a Versão 20.2 do `pip <http://pip-installer.org>` instalada.


Instalação
------------

Com o Python e Pip instalados,
Instale o robotframework::

    pip install robotframework

Instale o robotframework-seleniumlibrary::

    pip install --upgrade robotframework-seleniumlibrary

Instale o robotframework-seleniumlibrary (Biblioteca para geração de dados randomicos)::

    pip install robotframework-faker   


Executando
-----

Testes são executados a partir da linha de comando usando  ``robot``

Para executar todos os testes desse projeto basta executar o comando::

    robot .\Teste_Robot_Selenium


Tambem é possivel executar só uma suite, basta acrecentar o caminho::

    robot '.\Teste_Robot_Selenium\1_Testes de validacao de campos.robot'

Ou ::

    robot '.\Teste_Robot_Selenium\2_Testes de selecao de produto.robot' 

 




