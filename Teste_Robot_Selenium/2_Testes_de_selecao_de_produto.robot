***Settings***
Documentation       Cenários de testes de selecao de produto no site americanas.com
Resource    ./Resource_Keywords.robot

***Test Cases***
Teste de adicao de dois produtos na cesta
    Abrir Sessao    ${BROWSERS.CHROME}
    Fechar alerta de cookie
    Selecionar primeiro produto
    Selecionar segundo produto
    Validar valor total
    Validar parcelas
    Fechar sessao 